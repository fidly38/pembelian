<?php 

    class Detail extends CI_Model
    {
        function __construct()
        {
            $this->load->database();
        }

        function dataDetail()
        {
            $query = $this->db->get('detail');
            return $query->result();
        }
    }

?>