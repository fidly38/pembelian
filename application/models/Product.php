<?php

    class Product extends CI_Model
    {
        function __construct()
        {
            $this->load->database();
        }

        public function dataproduct()
        {
            $query = $this->db->get('product');
            return $query->result();  
        }

        public function insertproduct($data)
        {
            return $this->db->insert('product', $data);
        }

        public function getProductById($id)
        {
            return $this->db->get_where('product', array('id_product' => $id))->row();
        }

        public function updateproduct($product_id, $data)
        {
            $this->db->where('id_product', $product_id);
            return $this->db->update('product', $data);
        }

        public function get_status($productId)
        {
            $this->db->select('app_status');
            $this->db->where('id_product', $productId);
            $query = $this->db->get('product');

            if ($query->num_rows() > 0) {
                return ($query->row()->app_status === 't');
            } else {
                return null;
            }
        }

        public function update_status($productId, $newStatus)
        {
            $data = array(
                'app_status' => ($newStatus ? 't' : 'f')
            );
        
            $this->db->where('id_product', $productId);
            return $this->db->update('product', $data);
        }

    }

?>