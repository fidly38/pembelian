<?php

class Supplier extends CI_Model
{
    function __construct()
    {
        $this->load->database();
    }

    public function dataSupplier()
    {
        $query = $this->db->get('supplier');
        return $query->result();
    }

    public function getSupplierById($id)
    {
        return $this->db->get_where('supplier', array('id_supplier' => $id))->row();
    }

    public function insertSupplier($data)
    {
        return $this->db->insert('supplier', $data);
    }

    public function updateSupplier($supplier_id, $data)
    {
        $this->db->where('id_supplier', $supplier_id);
        return $this->db->update('supplier', $data);
    }

    public function get_status($supplierId)
    {
        $this->db->select('app_status');
        $this->db->where('id_supplier', $supplierId);
        $query = $this->db->get('supplier');

        if ($query->num_rows() > 0) {
            return ($query->row()->app_status === 't');
        } else {
            return null;
        }
    }

    public function update_status($supplierId, $newStatus)
    {
        $data = array(
            'app_status' => ($newStatus ? 't' : 'f')
        );
    
        $this->db->where('id_supplier', $supplierId);
        return $this->db->update('supplier', $data);
    }
}
