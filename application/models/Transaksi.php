<?php 

    class Transaksi extends CI_Model
    {
        function __construct()
        {
            $this->load->database();
        }

        public function dataTransaksi()
        {
            $query = $this->db->get('transaksi');
            return $query->result();
        }

        function simpanDetailTransaksi($transaksi_id, $data)
        {
            // // Simpan data ke tabel "transaksi"
            // $this->db->insert('transaksi', $data);
            // // Mendapatkan ID transaksi yang baru saja disimpan
            // $transaksi_id = $this->db->insert_id();

            // // Simpan data ke tabel "detail" dengan mengaitkannya dengan transaksi utama
            // $detail_data = array(
            //     'id_document' => $transaksi_id,
            //     'id_product' =>  $id_product,
            //     'v_unit_price' => $v_unit_price,
            //     'qty' => $qty,
            //     // Anda dapat menambahkan kolom-kolom lain yang sesuai di sini
            // );
            // $this->db->insert('detail', $detail_data);

            // // Mengembalikan ID transaksi yang baru saja disimpan
            // return $transaksi_id;
        }

        public function insertTransaksi($data)
        {
            return $this->db->insert('transaksi', $data);
            return $this->db->insert_id(); 
        }

        public function insertDetail($data)
        {
            return $this->db->insert('detail', $data);
        }

        public function getTransaksiByIdDocument($id_document)
        {
            return $this->db->get_where('transaksi', array('id_document' => $id_document))->row();
        }

        public function getDetailByIdDocument($id_document)
        {
            return $this->db->get_where('detail', array('id_document' => $id_document))->result();
        }

        public function get_status($transaksiId)
        {
            $this->db->select('app_status');
            $this->db->where('id_document', $transaksiId);
            $query = $this->db->get('transaksi');

            if ($query->num_rows() > 0) {
                return ($query->row()->app_status === 't');
            } else {
                return null;
            }
        }

        public function update_status($transaksiId, $newStatus)
        {
            $data = array(
                'app_status' => ($newStatus ? 't' : 'f')
            );
        
            $this->db->where('id_document', $transaksiId);
            return $this->db->update('transaksi', $data);
        }
    }

?>