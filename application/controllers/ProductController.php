<?php

    class ProductController extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('Product');
            $this->load->helper('url');
        }

        public function index()
        {
            $product['product'] = $this->Product->dataproduct();
            $this->load->view('product/indexproduct', $product);
        }

        public function getDataProduct()
        {
            $product = $this->Product->dataproduct();
            header('Content-Type: application/json');
            echo json_encode($product);
        }
    

        public function tambah()
        {
            $this->load->view('product/tambahproduct');
        }

        public function edit($product_id)
        {
            $product['product'] = $this->Product->getProductById($product_id);
            $this->load->view('product/editproduct', $product);
        }

        public function lihat($product_id)
        {
            $product['product'] = $this->Product->getProductById($product_id);
            $this->load->view('product/lihatproduct', $product);
        }

        public function simpandata()
        {
            $i_product_id = $this->input->post('i_product_id');
            $e_product_name = $this->input->post('e_product_name');
            $app_status = $this->input->post('app_status');
            
            if($app_status == 'aktif'){
                $app_status = true;
            } else {
                $app_status = false;
            }

            $data = [
                'i_product_id' => $i_product_id,
                'e_product_name' => $e_product_name,
                'app_status' => $app_status
            ];
            
            $this->Product->insertproduct($data, 'product');
            redirect('product');
        }

        public function simpanedit($product_id)
        {
            $data = array(
                'i_product_id' => $this->input->post('i_product_id'),
                'e_product_name' => $this->input->post('e_product_name'),
            );
            echo "product ID: " . $product_id . "<br>";
            echo "i_product_id: " . $data['i_product_id'] . "<br>";
            echo "e_product_name: " . $data['e_product_name'] . "<br>";
    
            $this->Product->updateproduct($product_id, $data);
        
            redirect('product');
        }

        public function change_status($productId)
        {
            $currentStatus = $this->Product->get_status($productId);
            if ($currentStatus == true) {
                $newStatus = false;
            } else {
                $newStatus = true;
            }

            if ($this->Product->update_status($productId, $newStatus)) {
                echo json_encode(array('status' => 'success', 'new_status' => $newStatus));
            } else {
                echo json_encode(array('status' => 'error'));
            }
        }


    }
?>