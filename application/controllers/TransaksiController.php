<?php 

    class TransaksiController extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('Transaksi');
            $this->load->model('Supplier');
            $this->load->model('Detail');
            $this->load->model('Product');
            $this->load->helper('url');
        }

        public function index()
        {
            $this->db->select('transaksi.*, supplier.e_supplier_name');
            $this->db->from('transaksi');
            $this->db->join('supplier', 'transaksi.id_supplier = supplier.id_supplier', 'left');
            $query = $this->db->get();
            $data['transaksi'] = $query->result();
            $this->load->view('transaksi/indextransaksi', $data);
        }

        public function tambah()
        {
            $data['transaksi'] = $this->Transaksi->dataTransaksi();
            $data['detail'] = $this->Detail->dataDetail();
            $data['supplier'] = $this->Supplier->dataSupplier();
            $data['product'] = $this->Product->dataproduct();
            $this->load->view('transaksi/tambahtransaksi', $data);
        }

        public function simpandata()
        {
            // Ambil data dari form untuk tabel "transaksi"
            $data_transaksi = array(
                'id_supplier' => $this->input->post('id_supplier'),
                'tanggal' => date('Y-m-d', strtotime($this->input->post('tanggal'))),
                'keterangan' => $this->input->post('keterangan'),
                'app_status' => true
            );

            // Memulai transaksi database
            $this->db->trans_start();

            // Simpan data ke tabel "transaksi"
            $this->Transaksi->insertTransaksi($data_transaksi);

            // Mendapatkan ID transaksi yang baru saja disimpan
            $transaksi_id = $this->db->insert_id();

            // Simpan data ke tabel "detail" dengan "id_document" yang sesuai
            $id_product = $this->input->post('id_product');
            $v_unit_price = str_replace(',', '', $this->input->post('v_unit_price'));
            $qty = str_replace(',', '', $this->input->post('qty'));

            // Menggunakan nilai id_document yang baru saja di-generate
            $id_document = $transaksi_id;

            for ($i = 0; $i < count((array)$id_product); $i++) {
                $detail_data = array(
                    'id_document' => $id_document, // Menggunakan id_document yang sama dengan transaksi
                    'id_product' => $id_product[$i],
                    'v_unit_price' => $v_unit_price[$i],
                    'qty' => $qty[$i]
                );
                $this->Transaksi->insertDetail($detail_data);
            }

            // Menandai akhir transaksi database
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                echo 'Transaksi database gagal: ' . $this->db->error();
            } else {
                redirect('transaksi');
            }
        }

        public function edit($id_document)
        {
            $data['transaksi'] = $this->Transaksi->getTransaksiByIdDocument($id_document);
            $data['detail'] = $this->Transaksi->getDetailByIdDocument($id_document);
            // var_dump($data['detail']);
            $data['supplier'] = $this->Supplier->dataSupplier();
            $data['product'] = $this->Product->dataproduct();
            $this->load->view('transaksi/edittransaksi', $data);
        }

        public function simpanedit()
        {
            
        }

        public function change_status($transaksiId)
        {
            $currentStatus = $this->Transaksi->get_status($transaksiId);
            if ($currentStatus == true) {
                $newStatus = false;
            } else {
                $newStatus = true;
            }

            if ($this->Transaksi->update_status($transaksiId, $newStatus)) {
                echo json_encode(array('status' => 'success', 'new_status' => $newStatus));
            } else {
                echo json_encode(array('status' => 'error'));
            }
        }
        
    }

?>