<?php

class SupplierController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Supplier');
        $this->load->helper('url');
    }

    public function index()
    {
        $supp['supplier'] = $this->Supplier->dataSupplier();
        $this->load->view('supplier/indexsupplier',$supp);
    }

    public function tambah()
    {
        $this->load->view('supplier/tambahsupplier');
    }

    public function simpandata()
    {
        $i_supplier_id = $this->input->post('i_supplier_id');
        $e_supplier_name = $this->input->post('e_supplier_name');
        $app_status = $this->input->post('app_status'); 
        
        if ($app_status == 'aktif') {
            $app_status = true;
        } else {
            $app_status = false;
        }

        $data = array(
            'i_supplier_id' => $i_supplier_id,
            'e_supplier_name' => $e_supplier_name,
            'app_status' => $app_status
        );

        $this->Supplier->insertSupplier($data, 'supplier');
        redirect('supplier');
    }

    public function edit($supplier_id)
    {
        $data['supplier'] = $this->Supplier->getSupplierById($supplier_id);
        $this->load->view('supplier/editsupplier', $data);
    }

    public function lihat($supplier_id)
    {
        $data['supplier'] = $this->Supplier->getSupplierById($supplier_id);
        $this->load->view('supplier/lihatsupplier', $data);
    }

    public function simpanedit($supplier_id)
    {
        $data = array(
            'i_supplier_id' => $this->input->post('i_supplier_id'),
            'e_supplier_name' => $this->input->post('e_supplier_name'),
        );
        echo "Supplier ID: " . $supplier_id . "<br>";
        echo "i_supplier_id: " . $data['i_supplier_id'] . "<br>";
        echo "e_supplier_name: " . $data['e_supplier_name'] . "<br>";

        $this->Supplier->updateSupplier($supplier_id, $data);
    
        redirect('supplier');
    }

    public function change_status($supplierId)
    {
        $currentStatus = $this->Supplier->get_status($supplierId);
        if ($currentStatus == true) {
            $newStatus = false;
        } else {
            $newStatus = true;
        }

        if ($this->Supplier->update_status($supplierId, $newStatus)) {
            echo json_encode(array('status' => 'success', 'new_status' => $newStatus));
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }
}


