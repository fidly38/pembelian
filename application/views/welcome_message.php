<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Supplier</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <style>
        .navbar {
            padding: 20px;
        }
        .panel {
            padding: 40px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand active" href="<?php echo base_url('/'); ?>">Home</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link" aria-current="page" href="<?php echo base_url('supplier'); ?>">Supplier</a>
                <a class="nav-link" href="<?php echo base_url('product'); ?>">Barang</a>
                <a class="nav-link" href="<?php echo base_url('transaksi'); ?>">Transaksi</a> 
            </div>
        </div>
    </div>
</nav>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>    
</body>
</html>
