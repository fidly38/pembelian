<!DOCTYPE html>
<html>
<head>
    <title>Tambah Data Supplier</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/fontawesome/css/all.min.css'); ?>">
    <style>
        .row {
            padding: 40px; 
        }

        .form-group {
            margin-bottom: 15px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand active" href="<?php echo base_url('/'); ?>">Home</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link" aria-current="page" href="<?php echo base_url('supplier'); ?>">Supplier</a>
                <a class="nav-link" href="<?php echo base_url('product'); ?>">Barang</a>
                <a class="nav-link" href="<?php echo base_url('transaksi'); ?>">Transaksi</a> 
            </div>
        </div>
    </div>
</nav>
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <center><div class="panel-heading">Data Supplier</center>
                <div class="panel-body">
                    <form action="javascript:history.back()" method="post" enctype="" >
                        <input name="_method" type="hidden" value="PATCH">

                        <div class="form-group {{ $errors->has('i_supplier_id') ? ' has-error' : '' }}">
                            <label class="control-label">Kode Supplier</label>	
                            <input type="text" name="i_supplier_id" class="form-control" value="<?php echo $supplier->i_supplier_id ?>"  readonly>
                        </div>

                        <div class="form-group {{ $errors->has('e_supplier_name') ? ' has-error' : '' }}">
                            <label class="control-label">Nama Supplier</label>	
                            <input type="text" name="e_supplier_name" class="form-control" value="<?php echo $supplier->e_supplier_name ?>"  readonly>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">kembali</button>
                        </div>
                    </form>
                </div>
                </div>	
            </div>
        </div>
    </div>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>      
</body>
</html>