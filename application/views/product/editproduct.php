<!DOCTYPE html>
<html>
<head>
    <title>Tambah Data product</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/fontawesome/css/all.min.css'); ?>">
    <style>
        .row {
            padding: 40px; 
        }

        .form-group {
            margin-bottom: 15px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand active" href="<?php echo base_url('/'); ?>">Home</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link" aria-current="page" href="<?php echo base_url('supplier'); ?>">Supplier</a>
                <a class="nav-link" href="<?php echo base_url('product'); ?>">Barang</a>
                <a class="nav-link" href="<?php echo base_url('transaksi'); ?>">Transaksi</a> 
            </div>
        </div>
    </div>
</nav>
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <center><div class="panel-heading">Edit Data product</center>
                <div class="panel-body">
                    <form action="<?php echo site_url('product/simpanedit/' . $product->id_product); ?>" method="post" enctype="" >
                        <input name="_method" type="hidden" value="PATCH">

                        <div class="form-group {{ $errors->has('i_product_id') ? ' has-error' : '' }}">
                            <label class="control-label">Kode product</label>	
                            <input type="text" name="i_product_id" class="form-control" value="<?php echo $product->i_product_id ?>"  required>
                        </div>

                        <div class="form-group {{ $errors->has('e_product_name') ? ' has-error' : '' }}">
                            <label class="control-label">Nama product</label>	
                            <input type="text" name="e_product_name" class="form-control" value="<?php echo $product->e_product_name ?>"  required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
                </div>	
            </div>
        </div>
    </div>

<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>      
</body>
</html>