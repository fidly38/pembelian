<!DOCTYPE html>
<html>
<head>
    <title>Tambah Data Supplier</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/fontawesome/css/all.min.css'); ?>">
    <style>
        .card {
            padding: 40px;
        }
        .form-group {
            margin-bottom: 15px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand active" href="<?php echo base_url('/'); ?>">Home</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link" aria-current="page" href="<?php echo base_url('supplier'); ?>">Supplier</a>
                <a class="nav-link" href="<?php echo base_url('product'); ?>">Barang</a>
                <a class="nav-link" href="<?php echo base_url('transaksi'); ?>">Transaksi</a> 
            </div>
        </div>
    </div>
</nav>
    <section class="card">
        <div class="row">
            <div class="container">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <center><div class="panel-heading">Tambah Data Barang 
                            <!-- <div class="panel-title pull-right"><a href="#">Kembali</a></div> -->
                        </div></center>
                        <br>
                        <div class="panel-body">
                            <form action="<?php echo base_url(). 'barang/simpandata'; ?>" method="post">

                                <div class="form-group {{ $errors->has('i_product_id') ? ' has-error' : '' }}">
                                    <label class="control-label">Kode Product</label>	
                                    <input type="text" name="i_product_id" class="form-control" required >
                                </div>

                                <div class="form-group {{ $errors->has('e_product_name') ? ' has-error' : '' }}">
                                    <label class="control-label">Nama Product</label>	
                                    <input type="text" name="e_product_name" class="form-control" required >
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Status</label>
                                    <div class="form-check">
                                        <input type="radio" name="app_status" value="aktif" class="form-check-input" required>
                                        <label class="form-check-label">Aktif</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="radio" name="app_status" value="tidak_aktif" class="form-check-input" required>
                                        <label class="form-check-label">Tidak Aktif</label>
                                    </div>
                                </div>                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </section>

    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>      
</body>
</html>
