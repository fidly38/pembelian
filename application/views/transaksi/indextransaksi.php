<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Transaksi</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <style>
        .navbar {
            padding: 40px;
        }
        .panel {
            padding: 20px;
        }
        .card{
            padding: 20px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand" href="<?php echo base_url('/'); ?>">Home</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link" aria-current="page" href="<?php echo base_url('supplier'); ?>">Supplier</a>
                <a class="nav-link" href="<?php echo base_url('product'); ?>">Barang</a>
                <a class="nav-link active" href="<?php echo base_url('transaksi'); ?>">Transaksi</a> 
            </div>
        </div>
    </div>
</nav>
<section class="card">
    <div class="row">
        <div class="container-fluid">
            <div class="col-md-12">
                <div class="panel panel-primary">
                <div>DATA TRANSAKSI
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end"><a href="<?php echo base_url('transaksi/tambah'); ?>" class="btn btn-primary">Tambah Data</a></div><br>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                    <table id="datatables" class="table table-striped">
                        <br>
                        <thead>
                            <tr>
                            <th>No</th>
                            <th>Nama Supplier</th>
                            <th>Tanggal</th>
                            <th>keterangan</th>
                            <th>Status</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; 
                            foreach($transaksi as $data): ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
                                <td><?php echo $data->e_supplier_name ?></td>
                                <td><?php echo date('d-m-Y', strtotime($data->tanggal)); ?></td>
                                <td><?php echo $data->keterangan ?></td>
                                <td>
                                    <?php if ($data->app_status == "t"){ ?>
                                        <button class="badge btn btn-primary" id="status_<?php echo $data->id_document; ?>" onclick="toggleStatus(<?php echo $data->id_document; ?>)">
                                        Aktif</button>
                                    <?php } elseif($data->app_status == "f"){ ?>
                                        <button class="badge btn btn-warning" id="status_<?php echo $data->id_document; ?>" onclick="toggleStatus(<?php echo $data->id_document; ?>)">
                                        Tidak Aktif</button>
                                        <?php } else { ?>
                                            <button class="badge btn btn-danger" id="status_<?php echo $data->id_document; ?>" onclick="toggleStatus(<?php echo $data->id_document; ?>)">
                                        Null</button>
                                    <?php } ?>
                                </td>
                                <td>
                                    <a class="fa fa-pencil btn btn-warning" href="<?php echo site_url('transaksi/edit/' . $data->id_document); ?>"></a>
                                    <a class="fa fa-eye btn btn-success" href="<?php echo site_url('transaksi/' . $data->id_document); ?>"></a>
                                </td>
                            </tr>
                            <?php endforeach; ?>  
                        </tbody>
                    </table>
                    </div>
                </div>
                </div>  
            </div>
        </div>
    </div>
</section>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable();
    });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
    function toggleStatus(trasaksiId) {
        Swal.fire({
            title: 'Konfirmasi',
            text: 'Anda yakin ingin mengubah status?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Ya, Ubah',
            cancelButtonText: 'Batal',
        }).then((result) => {
            if (result.isConfirmed) {
                var statusBadge = document.getElementById('status_' + trasaksiId);
                var currentStatus = statusBadge.innerHTML.trim();
                $.ajax({
                    url: '<?php echo site_url('transaksi/change_status/'); ?>' + trasaksiId,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        status: (currentStatus === 'Aktif')
                    },
                    success: function (response) {
                        if (response.status === 'success') {
                            if (response.new_status === true) {
                                statusBadge.innerHTML = 'Aktif';
                                statusBadge.classList = 'badge btn btn-primary'
                            } else {
                                statusBadge.innerHTML = 'Tidak Aktif';
                                statusBadge.classList = 'badge btn btn-warning'
                            }
                            Swal.fire('Status Berhasil Diubah', '', 'success');
                        } else {
                            Swal.fire('Terjadi kesalahan', 'Status gagal diubah', 'error');
                        }
                    },
                    error: function (error) {
                        Swal.fire('Kesalahan', 'Gagal mengubah status di database', 'error');
                    }
                });
            }
        });
    }
</script>
<!-- <script type="text/javascript">
    $('#select2').select2({
    placeholder: 'Pilih Supplier',
    ajax: {
        url: '/search',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
        return {
            results: data
        };
        },
        cache: true
    }
    });
</script> -->
</body>
</html>
