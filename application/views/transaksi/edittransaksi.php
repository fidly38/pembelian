<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Transaksi</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <!-- DATE PICKER -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <style>
        .navbar {
            padding: 40px;
        }
        .panel {
            padding: 20px;
        }
        .card{
            padding: 20px;
            margin-top: 20px;
            margin-left: 20px;
            margin-right: 20px;
        }
        .custom {
            height: 30px;
        }
        .ukuranform{
            height: 30px;;
        }
        .ukurangrandtotal{
            margin-right: 30px;
        }
        .ukurangrandstrong{
            margin-left: 143px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand active" href="<?php echo base_url('/'); ?>">Home</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link" aria-current="page" href="<?php echo base_url('supplier'); ?>">Supplier</a>
                <a class="nav-link" href="<?php echo base_url('product'); ?>">Barang</a>
                <a class="nav-link active" href="<?php echo base_url('transaksi'); ?>">Transaksi</a>
            </div>
        </div>
    </div>
</nav>
<form action="<?php echo base_url() . 'transaksi/simpandata'; ?>" method="post">
    <section class="card">
        <div class="container-fluid">
            <div class="row">
                <figure class="text-left">
                    <blockquote class="blockquote">
                        <p>TRANSAKSI</p>
                    </blockquote>
                </figure>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Nama Supplier</label>
                        <select name="id_supplier" id="select2" class="form-control select2" required>
                            <option value="">---</option>
                            <?php foreach ($supplier as $data): ?>
                                <option value="<?php echo $data->id_supplier; ?>" <?php echo ($data->id_supplier == $transaksi->id_supplier) ? 'selected' : ''; ?>>
                                    <?php echo $data->i_supplier_id . ' - ' . $data->e_supplier_name; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('tanggal') ? ' has-error' : '' }}">
                        <label class="control-label">Tanggal</label>
                        <input type="text" name="tanggal" id="datepicker" class="form-control" value="<?php echo $transaksi->tanggal; ?>" required>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('keterangan') ? ' has-error' : '' }}">
                        <label class="control-label">Keterangan</label>
                        <textarea type="text" name="keterangan" class="form-control custom"><?php echo $transaksi->keterangan; ?></textarea>
                    </div>
                </div>
                
            </div>
            <br>
            <div class="form-group">
                <a href="<?php echo base_url('transaksi'); ?>" type="button" class="btn btn-outline-dark">Kembali</a>
                <button type="submit" class="btn btn-outline-primary">Simpan</button>
            </div>
        </div>
    </section>
    <br>
    <section class="card">
        <div class="container-fluid">
            <div class="row">
                <figure class="text-left">
                    <blockquote class="blockquote">
                        <p>DETAIL TRANSAKSI&nbsp;&nbsp;
                            <button class="btn btn-success fa fa-plus " type="button" name="tambahdetailproduk" id="tambahdetailproduk" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">
                            </button></p>
                    </blockquote>
                </figure>
                <table class="table table-bordered" id="addtable">
                    <tr>
                        <th>No</th>
                        <th>Nama Product</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Total</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    $no=1; 
                    foreach ($detail as $item): ?>
                        <tr>
                            <td>
                                <?php echo $no++ ?>
                            </td>
                            <td>
                                <select name="id_product[]" class="form-control select2" required>
                                    <option value="">---</option>
                                    <?php foreach ($product as $data): ?>
                                        <option value="<?php echo $data->id_product; ?>" <?php echo ($data->id_product == $item->id_product) ? 'selected' : ''; ?>>
                                            <?php echo $data->i_product_id . ' - ' . $data->e_product_name; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <input type="text" name="v_unit_price[]" class="form-control number-format ukuranform" value="<?php echo $item->v_unit_price; ?>" required>
                            </td>
                            <td>
                                <input type="text" onkeydown="return event.keyCode !== 69" name="qty[]" class="form-control number-format ukuranform" value="<?php echo $item->qty; ?>" required>
                            </td>
                            <td>
                                <input type="text" name="total[]" class="form-control ukuranform" readonly>
                            </td>
                            <td>
                                <input class="btn btn-outline-danger hapusdetailproduk" type="button" value="Hapus">
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </table>
            </div>
            <div class="row g-3">
                <div class="col-sm-7"></div>
                <div class="col-sm">
                    <strong class="ukurangrandstrong">GRAND TOTAL &nbsp;&nbsp;&nbsp;:</strong>
                </div>
                <div class="col-sm">
                    <input type="text" id="grandTotal" class="form-control ukurangrandtotal ukuranform" readonly>
                </div>
            </div>
        </div>
    </section>
</form>
<!-- LINK SCRIPT -->
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- DATE PICKER -->
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script>
$(function() {
    $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        "setDate": new Date(),
        dateFormat: 'dd-mm-yy'
    });
});
</script>

<!-- SELECT 2 -->
<script>
    $(document).ready(function () {
        $('.select2').select2();
    });
</script>

<!-- FORMAT NUMBER -->
<script>
    // Format angka saat input
    document.addEventListener("input", function (e) {
        if (e.target.classList.contains('number-format')) {
            var cleanedValue = e.target.value.replace(/,/g, ''); // Hapus koma dari nilai
            e.target.value = formatNumber(cleanedValue);
            calculateTotal(e.target.closest('tr')); // Hitung total saat input berubah
        }
    });
    // Fungsi untuk memformat angka dengan koma
    function formatNumber(number) {
        var formatted = number.replace(/\D/g, ''); // Menghilangkan karakter selain digit
        formatted = formatted.replace(/\B(?=(\d{3})+(?!\d))/g, ','); // Menambahkan koma setiap 3 digit dari belakang
        return formatted;
    }
</script>

<!-- ADD KOLOM DETAIL TRANSAKSI -->
<script>
    $(document).ready(function () {
    var max = 4;
    var x = 1;
    var selectedProducts = []; // Objek untuk melacak produk yang dipilih

    $("#tambahdetailproduk").click(function () {
        if (x < max) {
            var html = '<tr>' +
                '<td class="no">' + (x + 1) + '</td>' +
                '<td><select name="id_product[]" class="form-control select2" required></select></td>' +
                '<td><input type="text" name="v_unit_price[]" class="form-control number-format ukuranform" required></td>' +
                '<td><input type="text" onkeydown="return event.keyCode !== 69" name="qty[]" class="form-control number-format ukuranform" required></td>' +
                '<td><input type="text" name="total[]" class="form-control ukuranform" readonly></td>' +
                '<td><input class="btn btn-outline-danger hapusdetailproduk" type="button" value="Hapus"></td>' +
                '</tr>';

            var newRow = $(html);
            var selectProduct = newRow.find("select");

            // Tambahkan pilihan produk yang belum dipilih
            refreshProductOptions(selectProduct);

            // Tambahkan baris ke tabel
            $("#addtable").append(newRow);
            x++;
            $('.select2').select2();
            updateNo();
        } else {
            alert("Maksimum 5 baris sudah tercapai.");
        }
    });

    function updateNo() {
        $("#addtable tr").each(function (index) {
            $(this).find(".no").text(index ++);
        });
    }

    $("#addtable").on('click', '.hapusdetailproduk', function () {
        var selectedProduct = $(this).closest('tr').find("select").val();

        // Periksa apakah produk sebelumnya diubah dan dapat dipilih kembali
        if (selectedProduct !== "") {
            delete selectedProducts[selectedProduct];
        }

        $(this).closest('tr').remove();
        x--;
        updateNo();
    });

    // Fungsi untuk memperbarui opsi produk
    function refreshProductOptions(selectProduct) {
        var selectOptions = '<option value="">---</option>';
        var products = <?php echo json_encode($product); ?>;
        products.forEach(function (product) {
            var productId = product.id_product;

            // validasi apakah produk sudah dipilih sebelumnya
            if (selectedProducts[productId] !== undefined) {
                selectOptions += '<option value="' + productId + '" disabled>' + product.i_product_id + ' - ' + product.e_product_name + ' (Sudah Dipilih)</option>';
            } else {
                selectOptions += '<option value="' + productId + '">' + product.i_product_id + ' - ' + product.e_product_name + '</option>';
            }
        });

        selectProduct.html(selectOptions);
    }

    // Event listener untuk memperbarui produk yang dipilih
    $("#addtable").on('change', 'select', function () {
        var selectedProduct = $(this).val();
        var previousProduct = $(this).data('prevProduct');

        // Periksa apakah ada produk sebelumnya yang diubah
        if (previousProduct && previousProduct !== selectedProduct) {
            // Produk sebelumnya dapat dipilih kembali
            delete selectedProducts[previousProduct];
        }

        if (selectedProduct) {
            // Periksa apakah produk sudah dipilih sebelumnya
            if (selectedProducts[selectedProduct] !== undefined) {
                alert("Produk ini sudah dipilih sebelumnya.");
                $(this).val(selectOptions); // Pilih produk kembali
            } else {
                // Tambahkan produk ke daftar yang dipilih
                selectedProducts[selectedProduct] = true;
            }
        }
        // Simpan produk yang dipilih sebelumnya
        $(this).data('prevProduct', selectedProduct);
    });             
});

</script>

<!-- MENGHITUNG TOTAL -->
<script>
    // Fungsi untuk menghitung total baris
    function calculateTotal() {
    var totalInputs = document.querySelectorAll('[name="total[]"]');
    
    totalInputs.forEach(function(totalInput) {
        var row = totalInput.closest('tr');
        var price = row.querySelector('[name="v_unit_price[]"]').value.replace(/,/g, '') || 0;
        var qty = row.querySelector('[name="qty[]"]').value.replace(/,/g, '') || 0;
        var total = price * qty;
        totalInput.value = formatNumber(total.toString());
    });
    
    calculateGrandTotal();
}

// Pemanggilan calculateTotal saat halaman dimuat
document.addEventListener("DOMContentLoaded", function() {
    calculateTotal();
});


    // Fungsi untuk menghitung grand total
    function calculateGrandTotal() {
        var grandTotal = 0;
        var totalInputs = document.querySelectorAll('[name="total[]"]');
        totalInputs.forEach(function(totalInput) {
            var total = totalInput.value.replace(/,/g, ''); // Ambil nilai total dan hilangkan koma
            grandTotal += parseFloat(total) || 0;
        });

        // Tampilkan grand total
        document.getElementById("grandTotal").value = formatNumber(grandTotal.toFixed());
    }
        // Menghitung grand total saat halaman dimuat
        document.addEventListener("DOMContentLoaded", function () {
            calculateGrandTotal();
        });


        // Format angka dengan koma
    function decreaseGrandTotal(row) {
        var totalInput = row.querySelector('[name="total[]"]');
        var total = totalInput.value.replace(/,/g, ''); // Ambil nilai total dan hilangkan koma
        var grandTotalInput = document.getElementById("grandTotal");
        var grandTotal = grandTotalInput.value.replace(/,/g, ''); // Ambil nilai grand total dan hilangkan koma
        grandTotal -= parseFloat(total);
        // Tampilkan grand total yang sudah dikurangi
        grandTotalInput.value = formatNumber(grandTotal.toFixed());
    }
        // Event listener untuk perubahan pada kolom total
        document.addEventListener("click", function (e) {
            if (e.target.classList.contains('hapusdetailproduk')) {
                var row = e.target.closest('tr');
                decreaseGrandTotal(row);
                row.remove();
                updateNo();
            }
        });
</script>
</body>
</html>
